## E-commerce di opere d'arte basato su blockchain Ethereum 

Il progetto, realizzato per un esame universitario, si pone l'obiettivo di
implementare un semplice e-commerce di opere d'arte che permette di certificare
l'acquisto da parte di un cliente (identificato dal proprio indirizzo Ethereum)
di una specifica opera d'arte.

I file sono suddivisi fra front end (HTML, CSS, Javascript, Bootstrap, jQuery)
e back-end (Solidity, Truffle, Ganache).

Nei file è presente il file *Tutorial.pdf* in cui vengono illustrati passo per
passo i passaggi, il codice e le tecnologie utilizzate per la realizzazione del
progetto.